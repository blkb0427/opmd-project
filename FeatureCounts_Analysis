---
title: "FeatureCounts Analysis for Conway et al. data sets"
author: "Bernard Lee"
date: "2019-12-31"
output: html_notebook
editor_options: 
  chunk_output_type: inline
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## 1. Setup
```{r}
library(Rsubread)
```

## 2. Read in the BAM files
```{r}
bam.files <- list.files(path = "~/OneDrive - Cancer Research Malaysia/ChaiPhei/CP_RNASeq/Conway_BAM/.", pattern = "*.bam", full.names = TRUE)
length(bam.files)
```

## 3. Starts featureCounts analysis
```{r}
fc_values <- featureCounts(files = bam.files, annot.inbuilt = "hg38", annot.ext = "~/OneDrive - Cancer Research Malaysia/ChaiPhei/CP_RNASeq/Conway_BAM/Data/gencode.v29.annotation.gtf", isGTFAnnotationFile = TRUE, GTF.featureTyp = "gene", GTF.attrType = "gene_name", isPairedEnd = TRUE, useMetaFeatures = TRUE, allowMultiOverlap = TRUE)
```

## 4. Get the count data and write the final output
```{r}
Counts <- as.data.frame(fc_values$counts)
write.table(Counts, "~/OneDrive - Cancer Research Malaysia/ChaiPhei/CP_RNASeq/Conway_BAM/Output/Conway_GE_original.txt", quote = FALSE, sep = '\t')
```